(async () => {
    myHeaders = new Headers();
    myHeaders.append("referer", "https://www.pixiv.net/");
     
    requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
     
    await fetch("https://i.pximg.net/img-master/img/2023/04/21/19/32/15/107383474_p0_master1200.jpg", requestOptions)
    .then(res => res.blob().then(blob => {
        image = new Image();
        image.src = URL.createObjectURL(blob);
        document.body.appendChild(image);
    }))
    })()