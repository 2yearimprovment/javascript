inputurl = prompt('enter the link');
myHeaders = new Headers();
(async () => {
    myHeaders = new Headers();
    myHeaders.append("referer", "https://www.pixiv.net/");

    requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(inputurl, requestOptions)
        .then(res => res.blob().then(blob => {
            reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function () {
                base64data = reader.result.replace(/data.*?base64,/, "");
                formData = new FormData();
                formData.append('image', base64data);
                formData.append('type', 'base64')
                fetch('https://api.imgur.com/3/image', {
                    method: 'POST',
                    headers: {
                        Authorization: 'Client-ID 90ef1830bd083ba',
                    },
                    body: formData
                }).then(data => data.json()).then(data => {console.log(data.data.link)})
            }
            image = new Image();
            image.src = URL.createObjectURL(blob);
            document.body.appendChild(image);
        }))
})()